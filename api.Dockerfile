FROM python:3.12

ARG GIT_HASH
ENV GIT_HASH=${GIT_HASH}

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /app

COPY requirements-api.txt /app/
RUN pip install -r requirements-api.txt --no-cache

COPY main.py /app/

EXPOSE 8000

CMD ["uvicorn", "--workers", "4", "--host", "0.0.0.0", "main:app"]
