FROM nvidia/cuda:12.4.1-runtime-ubuntu22.04

ARG GIT_HASH
ENV GIT_HASH=${GIT_HASH}

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /app

# RUN apt-get update && apt-get install -y --no-install-recommends gcc g++ make python3 python3-dev python3-pip python3-venv python3-wheel espeak espeak-ng libsndfile1-dev && apt-get clean && rm -rf /var/lib/apt/lists/*
RUN apt-get update && apt-get install -y --no-install-recommends gcc g++ make python3 python3-dev python3-pip python3-venv python3-wheel libsndfile1-dev ffmpeg && apt-get clean && rm -rf /var/lib/apt/lists/*

COPY requirements-worker.txt /app/
RUN pip install -r requirements-worker.txt --no-cache

COPY worker.py /app/

CMD ["arq", "worker.WorkerSettings"]
